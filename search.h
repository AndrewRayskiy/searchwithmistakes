#pragma once
#define _CRT_SECURE_NO_WARNINGS

#include <set>

#include "levenshtein.h"
#include "data.h"
#include "mistakes.h"
#include "noisy_channel.h"
#include <omp.h>
#include <ctime>

namespace search {
	int song_comparison(string s, string t) {
		auto s_splitted = split(s), t_splitted = split(t);

		int similarity = levenshtein::levenshtein(s, t, true).first;
		int n = s_splitted.size(), m = t_splitted.size();

		if (n > m) return similarity;

		vector<vector<int>> arr(n);
		for (int i = 0; i < n; i++)
			arr[i].resize(m + 1, levenshtein::INF);
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
				arr[i][j] = levenshtein::levenshtein(s_splitted[i], t_splitted[j], true).first;
		return std::min(similarity, levenshtein::hungarian_algorithm(n, m, arr));
	}


	vector<string> top_frequent(string s, int lvs_dist = 2) {
		return noisy_channel::calculate_all(s, lvs_dist);
	}


	vector<string> full_search(string s, int lvs_dist = 2) {
		std::cerr << "Accuracy is " << lvs_dist << std::endl;

		s = lower(s);
		auto words = split(s);

		int sz = words.size();
		vector<vector<string>> candidates(sz);

		clock_t t = clock();

#pragma omp parallel for
		for (int i = 0; i < sz; i++) {
			string word = words[i];
			vector<string> local_list;
			//auto time = get_time();
			vector<string> local_candidates = top_frequent(word, lvs_dist);
			//std::cerr << "another candidate found in " << (get_time() - time) << std::endl;
			for (string x : local_candidates)
				local_list.insert(local_list.end(), data::inverted_index[x].begin(), data::inverted_index[x].end());

			candidates.push_back(local_list);
		}

		std::cerr << "time for cand : " << (double)(clock() - t) / CLOCKS_PER_SEC << std::endl;

		if (candidates.size() == 0)
			return{};

		std::set<string> intersection = std::set<string>(data::songs.begin(), data::songs.end());

		/*for (auto &list : candidates)
		if (list.size() <= 100) {
		std::cout << "[ " << join(list) << " ]" << std::endl;
		}

		std::cout << endl;*/

		for (auto& inner_list : candidates)
			if (inner_list.size() > 5) {
				vector<string> leftover;
				for (string x : inner_list)
					if (intersection.count(x))
						leftover.push_back(x);
				intersection = std::set<string>(leftover.begin(), leftover.end());
			}

		int sz_res = intersection.size();
		vector<string> result(sz_res);
		vector<string> intersec = vector<string>(intersection.begin(), intersection.end());

#pragma omp parallel for
		for (int i = 0; i < sz_res; i++) {
			string t = intersec[i];
			if (song_comparison(s, t) <= lvs_dist)
				result[i] = t;
		}
		
		vector<string> rv;
		for (string s : result)
			if (s.size() > 0)
				rv.push_back(s);

		return rv;
	}
}
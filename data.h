#pragma once

#include "util.h"

namespace data {

	vector<string> songs;
	map<string, double> word_frequency;
	map<string, vector<string>> inverted_index;

	double total_length = 0;

	void run() {
		//freopen("db2.txt", "r", stdin);
		std::ifstream in("db.txt");
		string line;

		while (getline(in, line)) {
			auto splitted = split(line);
			for (string &s : splitted) s = lower(s);
			string lowercase_song = join(splitted);

			songs.push_back(lowercase_song);
			total_length += lowercase_song.size();

			for (string &s : splitted) {
				word_frequency[s]++;
				inverted_index[s].push_back(lowercase_song);
			}
		}

		for (auto& kvp : word_frequency)
			kvp.second /= total_length;

		in.close();
	}
};
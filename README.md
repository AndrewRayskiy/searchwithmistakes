Description
===========

This is a small project related to fuzzy search of songs.
For detailed explanations and complexity analysis refer to:

        principles.pdf
        complexity.pdf

Installation
------------

Download everything and use cmake/make to build the project.
        
        mkdir build
        cd build
        cmake CMakeLists.txt
        make

Quick Start
-----------

The following executables are available:

        ./demo        
Provides demo samples (for acquaintance).

        ./test   
Provides unit-testing utility.

        ./main      
Launches search of songs. Just enter several key words of the song and the tool will find all songs within certain distance from the given one.
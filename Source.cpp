#define _CRT_SECURE_NO_WARNINGS

#include <set>
#include <ctime>

#include "levenshtein.h"
#include "data.h"
#include "mistakes.h"
#include "noisy_channel.h"
#include "search.h"
#include <ctime>


void prepare() {
	data::run();
	mistakes::run();
}


double get_time() {
	return (double)clock() / CLOCKS_PER_SEC;
}


void pretty_print(string query, vector<string> results) {
	std::cout << "[Query]" << std::endl;
	std::cout << "-> " << query << std::endl;
	std::cout << std::endl;
	std::cout << "[Results]" << std::endl;
	for (string res : results)
		std::cout << "<- " << res << std::endl;
}


void launch_search(string s) {
	for (int accuracy : {1, 2}) {
		double time = get_time();
		auto result = search::full_search(s, accuracy);
		pretty_print(s, result);
		std::cerr << "full search conducted in " << (get_time() - time) << std::endl;
		std::cout << std::endl;
	}
	std::cout << std::endl;
}


int main() {
	std::cerr << "loading db." << std::endl;
	double time = get_time();
	prepare();
	std::cerr << "db init : " << (get_time() - time) << std::endl;

	clock_t start = clock();
	string s;
	std::cout << "Enter the song name :" << std::endl;
	while (getline(std::cin, s)) {
		launch_search(s);
		std::cout << "Enter the song name :" << std::endl;
	}
	clock_t end = clock();

	std::cerr << "total time is " << (double)(end - start) / CLOCKS_PER_SEC << std::endl;
}
#pragma once

#include <string>
#include <vector>
#include <algorithm>
#include <map>
#include <iostream>
#include <fstream>

using std::string;
using std::vector;
using std::map;


string lower(string s) {
	auto lowerize = [](char c) -> char {
		if (c < 65 || c > 90) return c;
		return (char)(c + 32);
	};
	for (char &c : s) c = lowerize(c);
	return s;
}


string join(vector<string> parts, char sep = ' ') {
	if (parts.size() == 0) {
		string s = "";
		s += sep;
		return s;
	}

	if (parts.size() == 1) {
		return parts[0];
	}

	string result = parts[0];
	for (int i = 1; i < parts.size(); i++)
		result += sep + parts[i];
	return result;
}


vector<string> split(string s, char sep = ' ') {
	if (s.size() == 0) return {};
	vector<string> result;

	string buffer = "";

	if (s[0] == sep) {
		result.push_back("");
		auto leftover = split(s.substr(1, s.size() - 1), sep);
		for (string s : leftover)
			result.push_back(s);
		return result;
	}
	else if (s.back() == sep) {
		auto leftover = split(s.substr(0, s.size() - 1), sep);
		for (string s : leftover)
			result.push_back(s);
		result.push_back("");
		return result;
	}

	for (char c : s)
		if (c != sep) {
			buffer.push_back(c);
		}
		else if (buffer.size() > 0) {
			result.push_back(buffer);
			buffer = "";
		}
	if (buffer.size() > 0) {
		result.push_back(buffer);
	}
	return result;
}
#pragma once

#include "levenshtein.h"
#include "data.h"
#include "mistakes.h"

#include <cmath>
#include <omp.h>

namespace noisy_channel {

	using std::pair;
	using std::make_pair;

	const double FREQUENCY_REDUCTION = 1e-4;
	const double ZERO_PROBABILITY = 1e-12;
	const double TOP = 15;
	const double FIX = 1e-1;
	const double INF = 1e18;


	double calculate_probs(string s, string t, int lvs_dist = 2) {
		auto lvs = levenshtein::levenshtein(s, t);

		int dist = lvs.first;
		if (dist > lvs_dist)
			return -INF;

		auto& backtrace = lvs.second;

		vector<Operation> operation_types;
		vector<string> changes;

		for (auto& logs : backtrace) {
			operation_types.push_back(logs.first);
			changes.push_back(logs.second);
		}

		double resulting_probability = log(data::word_frequency[t] * FREQUENCY_REDUCTION);

#pragma omp parallel for
		for (int i = 0; i < backtrace.size(); i++) {
			string left_part, right_part;
			auto splitted = split(changes[i], '-');
			if (splitted.size() == 1) {
				if (operation_types[i] == Operation::ADD)
					right_part = splitted[0];
				else
					left_part = splitted[0];
			}
			else {
				left_part = splitted[0];
				right_part = splitted[1];
			}

			if (mistakes::types[operation_types[i]].count(make_pair(left_part, right_part))) {
#pragma omp atomic
				resulting_probability += log(FIX * mistakes::types[operation_types[i]][make_pair(left_part, right_part)]
					/ mistakes::count[make_pair(left_part, right_part)]);
			}
			else
#pragma omp atomic
				resulting_probability += log(ZERO_PROBABILITY);
		}

		return resulting_probability;
	}


	vector<string> calculate_all(string s, int lvs_dist = 2) {
		vector<string> probs(data::inverted_index.size());
		vector<string> keys;
		for (auto& kvp : data::inverted_index) keys.push_back(kvp.first);

		int n = s.size();
		int sz = keys.size();

#pragma omp parallel for
		for (int i = 0; i < sz; i++) {
			string key = keys[i];
			if (abs(n - (int)key.size()) <= lvs_dist && levenshtein::lightweight_lvs(s, key) <= lvs_dist + 1)
				probs[i] = key;
		}

		vector<string> probs_res;
		for (string s : probs)
			if (s.size() > 0)
				probs_res.push_back(s);

		sort(probs_res.begin(), probs_res.end(), [&](string a, string b) {
			return calculate_probs(s, a, lvs_dist) > calculate_probs(s, b, lvs_dist);
		});

		if (probs_res.size() <= TOP)
			return probs_res;
		return vector<string>(probs_res.begin(), probs_res.begin() + TOP);
	}
};
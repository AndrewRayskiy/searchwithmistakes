#include "util.h"
#include "levenshtein.h"
#include "noisy_channel.h"
#include "data.h"
#include "mistakes.h"
#include "search.h"

using namespace std;

namespace test {
	bool compare(int test, vector<string> func_output, vector<string> expected_output) {
		if (func_output == expected_output) {
			std::cerr << "test #" << test << " passed" << std::endl;
			return true;
		}
		else {
			std::cerr << "test #" << test << " failed: { ";// << std::endl;
			std::cerr << "expected : ";
			for (auto& s : expected_output) std::cerr << s << " ";
			std::cerr << ", ";
			std::cerr << "got : ";
			for (auto& s : func_output) std::cerr << s << " ";
			std::cerr << "}" << std::endl;
			return false;
		}
	}
	bool lower_test() {
		vector<pair<string, string>> tests = {
			{"AbCde", "abcde"},
			{"aaaAXbbxbxbx", "aaaaxbbxbxbx"},
			{"1234yshs", "1234yshs"},
			{" ", " "},
			{"AxB", "axB"}
		};

		std::cerr << "testing /lower/" << std::endl;
		int test_num = 1;
		int passed = 0;
		for (auto& test : tests) {
			auto func_call = lower(test.first);
			if (compare(test_num, vector<string>({ func_call }), vector<string>({ test.second })))
				passed++;
			test_num++;
		}
		if (passed == tests.size()) {
			std::cerr << "testing finished successfully!" << std::endl << std::endl;
			return 1;
		}
		std::cerr << "testing finished with errors." << std::endl << std::endl;
		return 0;
	}

	bool join_test() {
		vector<pair<vector<string>, string>> tests = {
			{ vector<string>({"AbCde", "meow"}) , "AbCde meow"},
			{ vector<string>({"aaaAXbbxbxbx", "ayyy", "rkx"}) , "aaaAXbbxbxbx ayyy rkx" },
			{ vector<string>({"1234yshs"}), "1234yshs" },
		};

		std::cerr << "testing /join/" << std::endl;
		int test_num = 1;
		int passed = 0;
		for (auto& test : tests) {
			auto func_call = join(test.first);
			if (compare(test_num, vector<string>({ func_call }), vector<string>({ test.second })))
				passed++;
			test_num++;
		}
		if (passed == tests.size()) {
			std::cerr << "testing finished successfully!" << std::endl << std::endl;
			return 1;
		}
		std::cerr << "testing finished with errors." << std::endl << std::endl;
		return 0;
	}

	bool split_test() {
		vector<pair<string, vector<string>>> tests = {
			{ "AbCde meow", vector<string>({ "AbCde", "meow" })},
			{ "aaaAXbbxbxbx ayyy rkx", vector<string>({ "aaaAXbbxbxbx", "ayyy", "rkx" })},
			{ "1234yshs", vector<string>({ "1234yshs" })},
			{ " 1234", vector<string>({"", "1234"})},
			{ "1234 ", vector<string>({"1234", ""})}
		};

		std::cerr << "testing /split/" << std::endl;
		int test_num = 1;
		int passed = 0;
		for (auto& test : tests) {
			auto func_call = split(test.first);
			if (compare(test_num, func_call, test.second))
				passed++;
			test_num++;
		}
		if (passed == tests.size()) {
			std::cerr << "testing finished successfully!" << std::endl << std::endl;
			return 1;
		}
		std::cerr << "testing finished with errors." << std::endl << std::endl;
		return 0;
	}

	bool lightweight_lvs_test() {
		vector<pair<pair<string, string>, string>> tests = {
			{{"abcde", "accde"}, "1"},
			{{"it", "street"}, "5"},
			{{"sach", "schwach"}, "3"}
		};

		std::cerr << "testing /lightweight_lvs/" << std::endl;
		int test_num = 1;
		int passed = 0;
		for (auto& test : tests) {
			auto func_call = levenshtein::lightweight_lvs(test.first.first, test.first.second);
			if (compare(test_num, vector<string>({ to_string(func_call) }), vector<string>({ test.second })))
				passed++;
			test_num++;
		}
		if (passed == tests.size()) {
			std::cerr << "testing finished successfully!" << std::endl << std::endl;
			return 1;
		}
		std::cerr << "testing finished with errors." << std::endl << std::endl;
		return 0;
	}

	bool song_comparison_test() {
		vector<pair<pair<string, string>, string>> tests = {
			{{"abcde bcd eue", "abcdx eue bcb"}, "2"},
			{{"2pac let the be", "the beatles let it be"}, "4"},
			{{"imagine dagons", "dana roses imagine"}, "3"}
		};

		std::cerr << "testing /song_comparison/" << std::endl;
		int test_num = 1;
		int passed = 0;
		for (auto& test : tests) {
			auto func_call = search::song_comparison(test.first.first, test.first.second);
			if (compare(test_num, vector<string>({ to_string(func_call) }), vector<string>({ test.second })))
				passed++;
			test_num++;
		}
		if (passed == tests.size()) {
			std::cerr << "testing finished successfully!" << std::endl << std::endl;
			return 1;
		}
		std::cerr << "testing finished with errors." << std::endl << std::endl;
		return 0;
	}

	int main() {
		lower_test();
		join_test();
		split_test();
		lightweight_lvs_test();
		song_comparison_test();

		return 0;
	}
}

int main() {
	test::main();
	return 0;
}
#pragma once

#include "util.h"
#include "levenshtein.h"

using levenshtein::Operation;

namespace mistakes {

	using std::pair;
	using std::make_pair;

	map<Operation, map<pair<string, string>, int>> types;
	map<pair<string, string>, int> count;


	void run() {
		std::ifstream in("mistakes.txt");
		string line;
		while (getline(in, line)) {
			auto splitted = split(line);
			int frequency = stoi(splitted.back());

			//std::cerr << line << std::endl;
		
			auto error = split(join(vector<string>(splitted.begin(), splitted.end() - 1)), '|');

			if (error.size() < 2) continue;

			string old_str = error[0], new_str = error[1];

			//std::cerr << old_str << " " << new_str << std::endl;

			auto type = Operation::NONE;
			int n = old_str.size(), m = new_str.size();

			if (std::max(n, m) == 0) continue;

			if (n > m) {
				type = Operation::DELETE;
			}
			else if (n < m) {
				type = Operation::ADD;
			}
			else {
				if (n > 1 && old_str[0] == new_str[m - 1] && old_str[n - 1] == new_str[0])
					type = Operation::TRANSPOSE;
				else
					type = Operation::SUBSTITUTE;
			}

			for (int len_left = 1; len_left <= 2; len_left++)
				for (int len_right = 1; len_right <= 2; len_right++)
					for (int i = 0; i + len_left <= n; i++)
						for (int j = 0; j + len_right <= m; j++)
							count[make_pair(old_str.substr(i, len_left), new_str.substr(j, len_right))] += frequency;
		}
		in.close();
	}
}
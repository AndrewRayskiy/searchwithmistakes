#pragma once

#include "util.h"
#include <cassert>

namespace levenshtein {

	using std::pair;
	using std::make_pair;

	enum Operation {
		NONE = 0,
		ADD = 1,
		DELETE = 2,
		SUBSTITUTE = 3,
		TRANSPOSE = 4,
	};

	const int INF = 1000000000;


	int cost_insert(char c) {
		return 1;
	}


	int cost_delete(char c) {
		return 1;
	}


	int cost_substitute(char c, char t) {
		if (c == t)
			return 0;
		return 1;
	}


	int cost_transpose(char c, char t) {
		return 1;
	}


	int lightweight_lvs(string s, string t) {
		int n = s.size(), m = t.size();

		vector<vector<int>> dp(n + 1);
		for (int i = 0; i <= n; i++) dp[i].resize(m + 1, INF);

		dp[0][0] = 0;
		for (int i = 1; i <= n; i++)
			dp[i][0] = dp[i - 1][0] + cost_delete(s[i - 1]);
		for (int j = 1; j <= m; j++)
			dp[0][j] = dp[0][j - 1] + cost_insert(t[j - 1]);

		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= m; j++) {
				vector<int> res = {
					dp[i - 1][j] + cost_delete(s[i - 1]),
					dp[i][j - 1] + cost_insert(t[j - 1]),
					dp[i - 1][j - 1] + cost_substitute(s[i - 1], t[j - 1])
				};
				dp[i][j] = *min_element(res.begin(), res.end());
			}

		return dp[n][m];
	}


	struct state {
		int cost = INF;
		Operation op = Operation::NONE;
		vector<string> args;
		state() {}
		state(int cost, Operation op = Operation::NONE, vector<string> args = {}) :
			cost(cost), op(op), args(args) {}
		bool operator < (const state &other) {
			return cost < other.cost;
		}
	};


	string from_char(char c = '\0') {
		if (c == '\0')
			return "";
		string s = "";
		s += c;
		return s;
	}


	pair<int, vector<pair<Operation, string>>> levenshtein(string s, string t, bool no_trace = false) {
		int n = s.size(), m = t.size();

		vector<vector<state>> dp(n + 1);
		for (int i = 0; i <= n; i++) dp[i].resize(m + 1);

		dp[0][0] = state(0, Operation::NONE);

		for (int i = 1; i <= n; i++)
			dp[i][0] = state(dp[i - 1][0].cost + cost_delete(s[i - 1]), Operation::DELETE, { from_char(s[i - 1]) });
		for (int j = 1; j <= m; j++)
			dp[0][j] = state(dp[0][j - 1].cost + cost_insert(t[j - 1]), Operation::ADD, { from_char(t[j - 1]) });

		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= m; j++) {
				vector<state> results = {
					state(dp[i - 1][j - 1].cost + cost_substitute(s[i - 1], t[j - 1]), Operation::SUBSTITUTE, 
						vector<string>({from_char(s[i - 1]), from_char(t[j - 1])})),

					state(dp[i - 1][j].cost + cost_delete(s[i - 1]), Operation::DELETE, 
						vector<string>({i > 1 ? from_char(s[i - 2]) + from_char(s[i - 1]) : from_char(s[i - 1]), i > 1 ? from_char(s[i - 2]) : from_char()})),

					state(dp[i][j - 1].cost + cost_insert(t[j - 1]), Operation::ADD, 
						vector<string>({j > 1 ? from_char(t[j - 2]) : from_char(), j > 1 ? from_char(t[j - 2]) + from_char(t[j - 1]) : from_char(t[j - 1])})),

					(i > 1 && j > 1 && s[i - 1] == t[j - 2] && s[i - 2] == t[j - 1]) 
					? state(dp[i - 2][j - 2].cost + cost_transpose(s[i - 1], t[j - 1]), Operation::TRANSPOSE, 
						vector<string>({from_char(s[i - 1]), from_char(t[j - 1])}))
					: state()
				};
				dp[i][j] = *min_element(results.begin(), results.end());
			}

		if (no_trace)
			return make_pair(dp[n][m].cost, vector<pair<Operation, string>>());

		int x = n, y = m;
		vector<pair<Operation, string>> backtrace;

		while (x >= 1 || y >= 1) {
			auto op = dp[x][y].op;
			switch (op) {
				case (Operation::ADD):
					backtrace.push_back(make_pair(op, join(dp[x][y].args, '-')));
					y--;
					break;
				case (Operation::DELETE):
					backtrace.push_back(make_pair(op, join(dp[x][y].args, '-')));
					x--;
					break;
				case (Operation::SUBSTITUTE):
					if (dp[x][y].args[0] != dp[x][y].args[1])
						backtrace.push_back(make_pair(op, join(dp[x][y].args, '-')));
					x--, y--;
					break;
				case (Operation::TRANSPOSE):
				{
					string a = dp[x][y].args[0], b = dp[x][y].args[1];
					backtrace.push_back(make_pair(op, a + b + '-' + b + a));
					x -= 2, y -= 2;
					break;
				}
				default:
					assert(0);
			}
		}

		return make_pair(dp[n][m].cost, backtrace);
	}

	int hungarian_algorithm(int n, int m, vector<vector<int>> cost_array) {
		vector<vector<int>> arr(n + 1);
		for (int i = 0; i <= n; i++) arr[i].resize(m + 1);
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
				arr[i + 1][j + 1] = cost_array[i][j];

		vector<int> u(n + 1);
		vector<int> v(m + 1), p(m + 1);
		vector<int> way(m + 1);

		for (int i = 1; i <= n; i++) {
			p[0] = i;
			int j_wsp = 0;

			vector<int> minv(m + 1, INF);
			vector<int> used(m + 1);

			do {
				used[j_wsp] = 1;
				int i_wsp = p[j_wsp], delta = INF, j_overall = -1;
				for (int j = 1; j <= m; j++)
					if (used[j] == 0) {
						int curr = arr[i_wsp][j] - u[i_wsp] - v[j];
						if (curr < minv[j]) {
							minv[j] = curr, way[j] = j_wsp;
						}
						if (minv[j] < delta) {
							delta = minv[j], j_overall = j;
						}
					}
				for (int j = 0; j <= m; j++)
					if (used[j] > 0) {
						u[p[j]] += delta, v[j] -= delta;
					}
					else {
						minv[j] -= delta;
					}
				j_wsp = j_overall;
			} while (p[j_wsp] != 0);

			do {
				int j_overall = way[j_wsp];
				p[j_wsp] = p[j_overall];
				j_wsp = j_overall;
			} while (j_wsp);
		}
		return -v[0];
	}
}
